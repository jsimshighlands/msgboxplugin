# MsgBoxPlugin

## Plugin Overview

* Description:
    * Plugin designed to pop-up a message when field [CX.MSGBOX.TRIGGER] value is changed. In addition to popping up a message it will also log the users selected result.

* Git Repo:
    * [Bitbucket Repo](https://bitbucket.org/jsimshighlands/msgboxplugin/src/master/)
    * [Admin - Logging Repo](https://bitbucket.org/jsimshighlands/logform/src/master/)

* Dependent Fields:
    * CX.MSGBOX.TRIGGER
        * Used as the trigger field, populate this field using business rules to display a message box
    * CX.MSGBOX.TRIGGER.LOG
        * Used to log the users response and when they clicked that response

* Optional Forms:
    * Admin - Logging
        * This form provides access to the [CX.MSGBOX.TRIGGER] and [CX.MSGBOX.TRIGGER.LOG] fields along with the MsgBox Log button that shows all entries in the log in list view.

## How it works

When field CX.MSGBOX.TRIGGER is set to a string value, the MsgBoxPlugin parses that string, splitting it at the semi-colon character to create a message box to be displayed. This field accepts 1, 2 or 3 arguments to create a message box. See below for a sample string with all three arguments:

“This is the body of the message box.;Title of the message box;information”

This string will produce the following message box:

![Example 1](example1.jpg)

VB.Net line breaks (vbcrlf) and tabs (vbtab) are supported as normal. For example if the following advanced code is used in a field trigger rule:

```VB
Dim message As String = "Loan Information is:" + vbcrlf + vbTab + "Borr LN: " + [4002].ToString() + vbcrlf + vbTab + "Loan Amount: " + [2].ToString() + vbcrlf + vbTab + "Loan Officer: " + [317].ToString()

[cx.msgbox.trigger] = message + ";Loan Info;64"
```

The following message box would be displayed on trigger:

![Example 2](example2.jpg)

When an end user receives a message box and clicks on the OK or X button the results of that message box along with the users login ID, a timestamp of when they clicked, and title of the message box are recorded in the [CX.MSGBOX.TRIGGER.LOG].

### Argument Breakdown

Field [CX.MSGBOX.TRIGGER] accepts a string value up to 2000 characters. The MsgBoxPlugin will split the string in [CX.MSGBOX.TRIGGER] at the semi-colon (;) character. Up to 2 semi-colons can be used to pass 3 arguments to the C# Messagebox funtion in the MsgBoxPlugin. The arguments in order are:

1. The message of the message box.
1. The title of the message box.
1. The icon for the message box.

Out of these three only the message is required.

#### Argument 1: Message (required)

Any string before the first semi-colon or just entering a string with no semi-colon into [CX.MSGBOX.TIRGGER] will be placed in the body of the message box. The string passed can include line breaks (vbcrlf) and/or tabs (vbtab) aslong as it is a valid string in VB.net.

#### Argument 2: Title (optional)

Any string after the first semi-colon but before the second will be used as the message box title. This string is optional but recommended as the title of the message box is used for logging purposes. If no title portion is provided then a default title of "Informational pop-up" will be used. If not title is provided then the icon argument cannot be used.

#### Argument 3: Icon (optional)

Listed below are the options for icons using this message box, if a word or number is used that is not supported or if no title or icon argument is provided then the message box will not display any icon.

| Icon Argument       |              Resulting Icon              |
|---------------------|:----------------------------------------:|
| Information or 64   | ![Information Icon](informationicon.jpg) |
| Warning or 48       |     ![Warning Icon](warningicon.jpg)     |
| Question/Help or 32 |  ![Question/Help Icon](questionicon.jpg) |
| Error or 16         |       ![Error Icon](erroricon.jpg)       |
| No Icon             |          ![No Icon](noicon.jpg)          |

## Logging

Everytime a message box is produced using this plugin the results of that message box are recorded in the [CX.MSGBOX.TRIGGER.LOG] field using tab seperation. Below is an example of the log looks in its raw data from:

Information Icon	8/6/2018 9:33:57 AM	tuser	OK<br>
Warning Icon	8/6/2018 9:34:13 AM	tuser	OK<br>
Question/Help Icon	8/6/2018 9:34:40 AM	tuser	OK<br>
Error Icon	8/6/2018 9:35:47 AM	tuser	OK<br>
No Icon	8/6/2018 9:36:07 AM	tuser	OK

When viewed from using the MsgBox Log button on the Admin - Logging form, the raw data above is provied in a neat list view:

![Log View](logging.jpg)

Note that if no title is provided for the message box then Informational Pop-up will be logged as it is used as the default title when no title argument is provided.

![Informational Pop-up Log View](logginginfopupup.jpg)

## Issues/Workarounds

### Pop-up when file is opened

#### Overview

Currently if a pop-up is needed when a file is opened using a field trigger rule that triggers off of a custom field that captures the DateTime.Now() calculation allowed a message box to pop-up when a loan was opened or closed. It appears that when Encompass opens a loan file it first applies the business rules that match the condition for that loan and then activates the plugins. This means that pop-ups using the DateTime.Now() calculated custom field will not fire.

#### Workaround

Instead of using the DateTime.Now() calculated custom field, if another plugin provides an open trigger that sets a field when the loan is opened, use that as the trigger field. This will provide a pop-up when the loan is opened.

### Log is formatted strangely

#### Overview

The log formatting can be thrown off with a long message box title. This is due to the number of tabs that are used to try and align the headers of the log and the items in it.

#### Workaround

After copying and pasting the log correct the formatting.

## Future Improvement

1. Better Logging
    * create a custom form that allows for a ListView of the log.
        * Created Admin - Logging form to house this feature.
1. Additional Message Box Buttons
    * provide a way to allow for Yes/No/Cancel buttons to be used and actioned off of in business rules.
1. Input Message Box
    * prove a way to allow for a Input message box to be used and actioned off of in business rules.