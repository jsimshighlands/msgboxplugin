﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.ComponentModel;

namespace MsgBoxPlugin
{
    [Plugin]
    public class MsgBoxPlugin
    {
        public MsgBoxPlugin()
        {
            EncompassApplication.LoanOpened += EncompassApplication_LoanOpened;
            EncompassApplication.LoanClosing += EncompassApplication_LoanClosing;
        }

        public void EncompassApplication_LoanOpened(object sender, EventArgs e)
        {
            EncompassApplication.CurrentLoan.FieldChange += new FieldChangeEventHandler(MsgBoxTrigger_FieldChange);
        }

        public void MsgBoxTrigger_FieldChange(object source, FieldChangeEventArgs e)
        {
            // Only fire this plugin if the field that is changed is the MsgBox.Trigger field
            if (e.FieldID == "CX.MSGBOX.TRIGGER")
            {
                try
                {
                    var loan = EncompassApplication.CurrentLoan;

                    var msgBoxData = loan.Fields[e.FieldID].Value.ToString();

                    // Check to see if the MsgBox.Trigger field is not empty
                    if (!String.IsNullOrWhiteSpace(msgBoxData))
                    {

                        // Splits string provided in the trigger field into parts to use in the message box
                        var msgBoxParts = msgBoxData.Split(';');

                        var msgBoxBody = String.Empty;
                        var msgBoxTitle = String.Empty;
                        var msgBoxIcon = String.Empty;

                        // Depending on how many parts there are setting up the message box arguments.
                        switch (msgBoxParts.Length)
                        {
                            case 1:
                                msgBoxBody = msgBoxParts[0];
                                msgBoxTitle = "Informational Pop-up";
                                msgBoxIcon = "0";
                                break;

                            case 2:
                                msgBoxBody = msgBoxParts[0];
                                msgBoxTitle = msgBoxParts[1];
                                msgBoxIcon = "0";
                                break;

                            case 3:
                                msgBoxBody = msgBoxParts[0];
                                msgBoxTitle = msgBoxParts[1];
                                msgBoxIcon = msgBoxParts[2].Trim();
                                break;
                        }


                        // Setup a blank message box icon by default then parses, if available, icon from third argument
                        var msgBoxIconParse = MessageBoxIcon.None;

                        switch (msgBoxIcon.ToLower())
                        {
                            case "64":
                            case "information":
                                msgBoxIconParse = MessageBoxIcon.Information;
                                break;

                            case "48":
                            case "warning":
                                msgBoxIconParse = MessageBoxIcon.Warning;
                                break;

                            case "32":
                            case "question":
                                msgBoxIconParse = MessageBoxIcon.Question;
                                break;

                            case "16":
                            case "error":
                                msgBoxIconParse = MessageBoxIcon.Error;
                                break;

                            default:
                                msgBoxIconParse = MessageBoxIcon.None;
                                break;
                        }

                        // Creates and displays the message box based on the arguments above, captures returned result in res varaiable
                        var res = MessageBox.Show(msgBoxBody, msgBoxTitle, MessageBoxButtons.OK, msgBoxIconParse);

                        // Setup logging variable with information from current loan and message box result
                        var logEntry = msgBoxTitle + "\t" + DateTime.Now.ToString() + "\t" + loan.Session.UserID.ToString() + "\t" + res;

                        // Logs logging variable in field inside of loan, seperated by new line
                        if (loan.Fields["CX.MSGBOX.TRIGGER.LOG"].IsEmpty())
                        {
                            loan.Fields["CX.MSGBOX.TRIGGER.LOG"].Value = logEntry;

                        }
                        else
                        {
                            loan.Fields["CX.MSGBOX.TRIGGER.LOG"].Value = loan.Fields["CX.MSGBOX.TRIGGER.LOG"].Value + "\n" + logEntry;
                        }

                        loan.Fields["CX.MSGBOX.TRIGGER"].Value = String.Empty;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error in MsgBoxTrigger_FieldChange: {ex.ToString()}");
                }
            }
        }

        public void EncompassApplication_LoanClosing(object sender, EventArgs e)
        {
            EncompassApplication.CurrentLoan.FieldChange -= MsgBoxTrigger_FieldChange;
        }
    }
}
